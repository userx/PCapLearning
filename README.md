PCapLearning Project
=====================

 - The PCapLearning project aims to help those who are interested
in learning how libpcap works.

* This project will be just code snippets using libpcap.
* I will keep it up-to-date as soon as I learn, so the more I learn
more I'll update here.


P.S.:*Most of the code snippets found here are from:

http://homes.di.unimi.it/~gfp/SiRe/2002-03/progetti/libpcap-tutorial.html
http://www.tcpdump.org/pcap.htm

