#include <stdio.h>
#include <pcap.h>

/*
 * Step 1.
 * #################################################################
 * #                           Device Name                         #
 * #################################################################
 * 
 * * That's the way to take a default network device name
 */

int
main (int arhc, char ** argv) {
    char * dev;
    char errbuf[PCAP_ERRBUF_SIZE];

    dev = pcap_lookupdev (errbuf);

    if (dev == NULL) {
        fprintf (stderr, "Unable to find the default device: %s\n", errbuf);
        return 2;
    }

    printf ("Device: %s\n", dev);

    return 0;

}
